package com.creson.test.coffee.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by creson on 11/21/14.
 */
public class Coffee implements Serializable{

    private String lastUpdatedAt;
    private String desc;
    private String imageUrl;
    private String id;
    private String name;

    public String getLastUpdatedAt() {
        return lastUpdatedAt;
    }

    public void setLastUpdatedAt(String lastUpdatedAt) {
        this.lastUpdatedAt = lastUpdatedAt;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static class List extends ArrayList<Coffee> {
    }
}
