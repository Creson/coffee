package com.creson.test.coffee.ui.controllers;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.WindowManager;

import com.creson.test.coffee.CoffeeApplication;
import com.creson.test.coffee.R;
import com.creson.test.coffee.ui.views.SplashFragment;

/**
 * Created by creson on 11/21/14.
 */
public class SplashScreenActivity extends ActionBarActivity {

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private Runnable uiRunnable = new Runnable() {
        @Override
        public void run() {
//            editor = preferences.edit();
//            editor.putBoolean(CoffeeApplication.SHOW_SPLASH_TAG, false).commit();
            callMainActivity();
        }
    };
    private Handler backgroundHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash_screen);
        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.splash_fragment, SplashFragment.newInstance())
                    .commit();
        }
        preferences = getSharedPreferences(CoffeeApplication.APP_SHARED_PREFERENCE, MODE_PRIVATE);
        if (preferences.getBoolean(CoffeeApplication.SHOW_SPLASH_TAG, true)) {
            backgroundHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    runOnUiThread(uiRunnable);
                }
            }, 3000);
        } else {
            callMainActivity();
        }
    }

    public void callMainActivity() {
        Intent intent = new Intent(SplashScreenActivity.this, CoffeeListActivity.class);
        startActivity(intent);
        finish();
    }

}