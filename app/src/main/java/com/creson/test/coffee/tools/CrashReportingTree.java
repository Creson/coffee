package com.creson.test.coffee.tools;

import timber.log.Timber;

/**
 * Created by creson on 11/21/14.
 */
public class CrashReportingTree extends Timber.HollowTree {

    @Override
    public void i(String message, Object... args) {

    }

    @Override
    public void i(Throwable t, String message, Object... args) {
        i(message, args);
    }

    @Override
    public void e(String message, Object... args) {
        i("ERROR: " + message, args);
    }

    @Override
    public void e(Throwable t, String message, Object... args) {
        e(message, args);
    }
}
