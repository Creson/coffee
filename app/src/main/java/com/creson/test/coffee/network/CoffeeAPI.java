package com.creson.test.coffee.network;

import com.creson.test.coffee.model.Coffee;

import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by creson on 11/21/14.
 */
public abstract interface CoffeeAPI {

    @GET("/api/coffee/")
    Coffee.List coffees();

    @GET("/api/coffee/{item_id}")
    Coffee coffee(@Path("item_id") String itemId);
}