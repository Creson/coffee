package com.creson.test.coffee;

import android.app.Application;

import com.creson.test.coffee.tools.CrashReportingTree;

import timber.log.Timber;

/**
 * Created by creson on 11/21/14.
 */
public class CoffeeApplication extends Application {

    public static final String APP_SHARED_PREFERENCE = "CNewsClass";
    public static final String SHOW_SPLASH_TAG = "SHOW_SPLASH_TAG";

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new CrashReportingTree());
        }
    }
}
