package com.creson.test.coffee.network.request;

import com.creson.test.coffee.model.Coffee;
import com.creson.test.coffee.network.CoffeeAPI;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import roboguice.util.temp.Ln;

/**
 * Created by creson on 11/21/14.
 */
public class CoffeeDetailSpiceRequest extends RetrofitSpiceRequest<Coffee, CoffeeAPI> {

    private String itemId;

    public CoffeeDetailSpiceRequest(String itemId) {
        super(Coffee.class, CoffeeAPI.class);
        this.itemId = itemId;
    }

    @Override
    public Coffee loadDataFromNetwork() {
        Ln.d("Calling Coffee API for fetching Coffee List");
        return getService().coffee(itemId);
    }
}
