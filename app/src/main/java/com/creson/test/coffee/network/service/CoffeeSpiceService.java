package com.creson.test.coffee.network.service;

import com.creson.test.coffee.network.CoffeeAPI;

/**
 * Created by creson on 11/21/14.
 */
public class CoffeeSpiceService extends CustomRetrofitGsonSpiceService {

    public static final String API_KEY = "WuVbkuUsCXHPx3hsQzus4SE";
    private static final String BASE_URL = "https://coffeeapi.percolate.com";

    @Override
    public void onCreate() {
        super.onCreate();
        addRetrofitInterface(CoffeeAPI.class);
    }

    @Override
    protected String getServerUrl() {
        return BASE_URL;
    }
}
