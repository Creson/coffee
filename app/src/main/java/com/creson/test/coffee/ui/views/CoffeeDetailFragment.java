package com.creson.test.coffee.ui.views;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.creson.test.coffee.R;
import com.creson.test.coffee.ui.controllers.BaseFragment;

public class CoffeeDetailFragment extends BaseFragment {

    private static Context mContext;

    private View rootView;

    public CoffeeDetailFragment() {
        // Required empty public constructor
    }

    public static CoffeeDetailFragment newInstance(Context context) {
        CoffeeDetailFragment fragment = new CoffeeDetailFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        mContext = context;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_coffee_detail, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
