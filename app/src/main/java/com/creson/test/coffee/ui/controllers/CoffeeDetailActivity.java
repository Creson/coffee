package com.creson.test.coffee.ui.controllers;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.creson.test.coffee.R;
import com.creson.test.coffee.model.Coffee;
import com.creson.test.coffee.network.request.CoffeeDetailSpiceRequest;
import com.creson.test.coffee.tools.RequiredNetworkInstances;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

public class CoffeeDetailActivity extends BaseActivity {

    private Toolbar toolbar;
    private TextView actionBarTitleText;
    private TextView coffeeDetailTextView;
    private ImageView coffeeImgView;

    private Coffee localCoffee = new Coffee();

    private CoffeeDetailSpiceRequest coffeeDetailSpiceRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coffee_detail);


//        if (savedInstanceState == null) {
//            getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, CoffeeDetailFragment.newInstance(this)).commit();
//        }

        getExtrasFromBundle();
        setLayouts();
        setActionBar();
        fetchCoffeeDetail();
    }

    public void fetchCoffeeDetail() {
        coffeeDetailSpiceRequest = new CoffeeDetailSpiceRequest(localCoffee.getId());
        getSpiceManager().execute(coffeeDetailSpiceRequest, new RequestListener<Coffee>() {
            @Override
            public void onRequestFailure(SpiceException spiceException) {

            }

            @Override
            public void onRequestSuccess(Coffee coffee) {
                localCoffee = coffee;
                coffeeDetailTextView.setText(localCoffee.getDesc());
            }
        });
    }

    public void setLayouts() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        actionBarTitleText = (TextView) findViewById(R.id.coffee_title);
        coffeeDetailTextView = (TextView) findViewById(R.id.coffee_detail);
        coffeeImgView = (ImageView) findViewById(R.id.coffee_img);
        if (!localCoffee.getImageUrl().trim().equals("")) {
            RequiredNetworkInstances.getInstance(this).getPicasso().load(localCoffee.getImageUrl()).into(coffeeImgView);
        } else {
            coffeeImgView.setVisibility(ImageView.GONE);
        }
    }

    public void setActionBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setTitle("");
        actionBarTitleText.setText(localCoffee.getName());
    }

    public void getExtrasFromBundle() {
        if (getIntent().getExtras() != null) {
            localCoffee = (Coffee) getIntent().getExtras().getSerializable("Coffee");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_coffee_detail, menu);
        return true;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (item.getItemId()) {
            case android.R.id.home:
                if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP) {
                    finishAfterTransition();
                } else {
                    finish();
                }
                return true;
            case R.id.action_share:

                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                String textMessage = actionBarTitleText.getText() + "\n\n" + coffeeDetailTextView.getText();

                if (!localCoffee.getImageUrl().trim().equalsIgnoreCase("")) {
                    Drawable mDrawable = coffeeImgView.getDrawable();
                    Bitmap mBitmap = ((BitmapDrawable) mDrawable).getBitmap();

                    String path = MediaStore.Images.Media.insertImage(getContentResolver(),
                            mBitmap, "Image Description", null);

                    Uri uri = Uri.parse(path);

                    shareIntent.setType("text/images");

                    shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                } else {
                    shareIntent.setType("text/plain");
                }
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, actionBarTitleText.getText());
                shareIntent.putExtra(Intent.EXTRA_TEXT, textMessage);
                startActivity(shareIntent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
