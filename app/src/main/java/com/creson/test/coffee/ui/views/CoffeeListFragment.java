package com.creson.test.coffee.ui.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.creson.test.coffee.R;
import com.creson.test.coffee.adapter.CoffeeListAdapter;
import com.creson.test.coffee.model.Coffee;
import com.creson.test.coffee.network.request.CoffeeListSpiceRequest;
import com.creson.test.coffee.tools.ItemClickSupport;
import com.creson.test.coffee.ui.controllers.BaseFragment;
import com.creson.test.coffee.ui.controllers.CoffeeDetailActivity;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

public class CoffeeListFragment extends BaseFragment {

    private static Context mContext;

    private View rootView;
    private RecyclerView recyclerView;

    private Coffee.List coffeeList;

    private CoffeeListAdapter adapter;

    private CoffeeListSpiceRequest coffeeListSpiceRequest = new CoffeeListSpiceRequest();

    public CoffeeListFragment() {
        // Required empty public constructor
    }

    public static CoffeeListFragment newInstance(Context context) {
        CoffeeListFragment fragment = new CoffeeListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        mContext = context;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_coffee_list, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = (RecyclerView) view.findViewById(R.id.coffee_list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));

        setListeners();

        getSpiceManager().execute(coffeeListSpiceRequest, new RequestListener<Coffee.List>() {
            @Override
            public void onRequestFailure(SpiceException spiceException) {
//                Toast.makeText(mContext, "Download Failed", Toast.LENGTH_SHORT).show();
                Toast.makeText(mContext, "Error " + spiceException.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onRequestSuccess(Coffee.List coffees) {
                coffeeList = coffees;
                setAdapter();
            }
        });
    }

    public void setListeners() {
        ItemClickSupport itemClickSupport = ItemClickSupport.addTo(recyclerView);
        itemClickSupport.setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onItemClick(RecyclerView parent, View view, int position, long id) {

                Intent coffeeDetailIntent = new Intent(mContext, CoffeeDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("Coffee", coffeeList.get(position));
                coffeeDetailIntent.putExtras(bundle);
                CoffeeListAdapter.ViewHolder viewHolder = ((CoffeeListAdapter.ViewHolder) recyclerView.findViewHolderForPosition(position));

                ImageView sharedImgView = viewHolder.coffeeImg;
                TextView sharedTitle = viewHolder.coffeeTitle;

                ActivityOptionsCompat options = null;
                if (!coffeeList.get(position).getImageUrl().trim().equalsIgnoreCase("")) {
                    Pair[] pairs = new Pair[]{new Pair<View, String>(sharedImgView, getResources().getString(R.string.coffee_img_transition))
                            , new Pair<View, String>(sharedTitle, getResources().getString(R.string.coffee_title_transition))};
                    options = ActivityOptionsCompat
                            .makeSceneTransitionAnimation(getActivity()
                                    , pairs);
                } else {
                    options = ActivityOptionsCompat
                            .makeSceneTransitionAnimation(getActivity()
                                    , sharedTitle, getResources().getString(R.string.coffee_title_transition));
                }
                // start the new activity
                mContext.startActivity(coffeeDetailIntent, options.toBundle());
            }
        });
    }

    public void setAdapter() {
        adapter = new CoffeeListAdapter(mContext, coffeeList);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
