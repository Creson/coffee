package com.creson.test.coffee.network.request;

import com.creson.test.coffee.model.Coffee;
import com.creson.test.coffee.network.CoffeeAPI;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import roboguice.util.temp.Ln;

/**
 * Created by creson on 11/21/14.
 */
public class CoffeeListSpiceRequest extends RetrofitSpiceRequest<Coffee.List, CoffeeAPI> {


    public CoffeeListSpiceRequest() {
        super(Coffee.List.class, CoffeeAPI.class);
    }

    @Override
    public Coffee.List loadDataFromNetwork() {
        Ln.d("Calling Coffee API for fetching Coffee List");
        return getService().coffees();
    }
}
