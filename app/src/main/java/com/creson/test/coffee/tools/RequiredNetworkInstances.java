package com.creson.test.coffee.tools;

import android.content.Context;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.bind.DateTypeAdapter;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.util.Date;

/**
 * Created by creson on 11/21/14.
 */
public class RequiredNetworkInstances {

    private static RequiredNetworkInstances mInstances = null;
    private static Context context;

    private OkHttpClient okHttpClient = null;
    private Picasso picasso = null;
    private Gson gson = null;

    public static RequiredNetworkInstances getInstance(Context context) {
        if (mInstances == null) {
            mInstances = new RequiredNetworkInstances();
            RequiredNetworkInstances.context = context;
        }
        return mInstances;
    }

    public OkHttpClient getOkHttpClient() {
        if (okHttpClient == null) {
            okHttpClient = new OkHttpClient();
        }
        return okHttpClient;
    }

    public Picasso getPicasso() {
        if (picasso == null) {
            picasso = new Picasso.Builder(RequiredNetworkInstances.context)
                    .downloader(new OkHttpDownloader(getOkHttpClient()))
                    .build();
        }
        return picasso;
    }

    public Gson getGson() {
        if (gson == null) {
            gson = new GsonBuilder()
                    .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                    .registerTypeAdapter(Date.class, new DateTypeAdapter())
                    .create();
        }
        return gson;
    }
}
