package com.creson.test.coffee.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.creson.test.coffee.R;
import com.creson.test.coffee.model.Coffee;
import com.creson.test.coffee.tools.RequiredNetworkInstances;

import java.util.List;

/**
 * Created by creson on 11/29/14.
 */
public class CoffeeListAdapter extends RecyclerView.Adapter<CoffeeListAdapter.ViewHolder> {

    private Context context;
    private List<Coffee> coffeeList;

    public CoffeeListAdapter(Context context, List<Coffee> coffeeList) {
        this.context = context;
        this.coffeeList = coffeeList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.coffee_list_lay, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Coffee coffee = coffeeList.get(position);
        holder.coffeeTitle.setText(coffee.getName());
        holder.coffeeDesc.setText(coffee.getDesc());
        if (!coffee.getImageUrl().trim().equals("")) {
            holder.coffeeImg.setVisibility(ImageView.VISIBLE);
            RequiredNetworkInstances.getInstance(context).getPicasso().load(coffee.getImageUrl()).into(holder.coffeeImg);
        } else {
            holder.coffeeImg.setVisibility(ImageView.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return coffeeList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView coffeeImg;
        public View itemView;
        public TextView coffeeTitle;
        public TextView coffeeDesc;

        public ViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            coffeeImg = (ImageView) itemView.findViewById(R.id.coffee_img);
            coffeeTitle = (TextView) itemView.findViewById(R.id.coffee_title);
            coffeeDesc = (TextView) itemView.findViewById(R.id.coffee_desc);
        }
    }
}
