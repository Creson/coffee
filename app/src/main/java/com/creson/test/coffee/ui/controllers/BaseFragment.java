package com.creson.test.coffee.ui.controllers;

import android.support.v4.app.Fragment;

import com.creson.test.coffee.network.service.CoffeeSpiceService;
import com.octo.android.robospice.SpiceManager;

/**
 * Created by creson on 12/3/14.
 */
public class BaseFragment extends Fragment {

    private SpiceManager spiceManager = new SpiceManager(CoffeeSpiceService.class);

    @Override
    public void onStop() {
        spiceManager.shouldStop();
        super.onStop();
    }

    @Override
    public void onStart() {
        spiceManager.start(getActivity());
        super.onStart();
    }

    protected SpiceManager getSpiceManager() {
        return spiceManager;
    }
}
