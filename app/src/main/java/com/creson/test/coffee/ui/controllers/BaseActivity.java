package com.creson.test.coffee.ui.controllers;

import android.support.v7.app.ActionBarActivity;

import com.creson.test.coffee.network.service.CoffeeSpiceService;
import com.octo.android.robospice.SpiceManager;

/**
 * Created by creson on 12/6/14.
 */
public class BaseActivity extends ActionBarActivity {

    private SpiceManager spiceManager = new SpiceManager(CoffeeSpiceService.class);

    @Override
    public void onStop() {
        spiceManager.shouldStop();
        super.onStop();
    }

    @Override
    public void onStart() {
        spiceManager.start(this);
        super.onStart();
    }

    protected SpiceManager getSpiceManager() {
        return spiceManager;
    }
}
