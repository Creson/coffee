# Overview #

Coffee is the technical test Android Application developed to search for coffee's using [Coffee API](https://coffeeapi.percolate.com/) provided by [Percolate](http://percolate.com/). This application is for 4.x devices & up.It is targetted mostly for Android 5.0 Lollipop devices.

# Coffee App Description #

This application shows the list of coffee that are in the Coffee API and shows detail about each respective coffee. 

## Architecture of Coffee ##

Coffee is divided in three screens
* Splash Screen
* Coffee List Screen
* Coffee Detail Screen

### Splash Screen ###

It shows the prime logo of the application along with the name and a progress bar. For this particular version the splash screen is programmed to appear every time a user opens the app. The splash screen will stay on the screen for 3 seconds and after then it will redirect the user to the next screen i.e. Coffee List Screen.

### Coffee List Screen ###

As the name suggests it shows all the coffees that are available from the Coffee API. The list of Coffees are displayed in a neat an attractive design keeping the end user in mind. This screen leads to the Coffee Detail Screen for a respective Coffee that the user desires to view simply by selecting one of the coffee's that are in the Coffee List Screen.

### Coffee Detail Screen ###

It displays the detail of the selected coffee with a properly organized view. Users can also share that respective coffee information through social mediums like Gmail etc.

# Dependencies #

To complete this application following great libraries were used
* [RecyclerView](https://developer.android.com/reference/android/support/v7/widget/RecyclerView.html)
* [Cardview](https://developer.android.com/reference/android/support/v7/widget/CardView.html)
* [Robospice](https://github.com/stephanenicolas/robospice)
* [Retrofit](http://square.github.io/retrofit/)
* [Picasso](http://square.github.io/picasso/)
* [Timber](https://github.com/JakeWharton/timber)
* [Gson](https://code.google.com/p/google-gson/)
* [Materialish Progress](https://github.com/pnikosis/materialish-progress)
*[ItemClickSupport.java](https://github.com/lucasr/twoway-view/blob/master/core/src/main/java/org/lucasr/twowayview/ItemClickSupport.java)
*[ClickItemTouchListener.java](https://github.com/lucasr/twoway-view/blob/master/core/src/main/java/org/lucasr/twowayview/ClickItemTouchListener.java)

# Description of Code #

The application uses api_key as the authorization to fetch the data. This api_key has been send as a part of header information instead of sending it through as a parameter of GET request. You can see the implementation of the header in [CustomRetrofitGsonSpiceService.java](https://bitbucket.org/Creson/cafine/src/31fe5dec311be32efb69315e89520de1a428de4b/app/src/main/java/com/creson/test/coffee/network/service/CustomRetrofitGsonSpiceService.java?at=master)


```
#!android
//In CoffeeSpiceService class
public static final String API_KEY = "WuVbkuUsCXHPx3hsQzus4SE";

//Using RequestInterceptor object to add header
private RequestInterceptor requestInterceptor = new RequestInterceptor() {
        @Override
        public void intercept(RequestFacade request) {
            request.addHeader("Accept", "application/json");
            request.addHeader("Authorization", CoffeeSpiceService.API_KEY);
        }
    };

//retruning the RestAdapter object consisting of newly added RequestInterceptor object
protected RestAdapter.Builder createRestAdapterBuilder() {
        return new RestAdapter.Builder()
                .setRequestInterceptor(requestInterceptor)
                ...
    }

```

[CoffeeListSpiceRequest](https://bitbucket.org/Creson/cafine/src/5ccf9f5cd653639365e0e249b9f67247ced0d0bd/app/src/main/java/com/creson/test/coffee/network/request/CoffeeListSpiceRequest.java?at=master) class is used to request the Coffee list from [Coffee API](https://coffeeapi.percolate.com/api/coffee/)

```
#!android

public class CoffeeListSpiceRequest extends RetrofitSpiceRequest<Coffee.List, CoffeeAPI> {


    public CoffeeListSpiceRequest() {
        super(Coffee.List.class, CoffeeAPI.class);
    }

    @Override
    public Coffee.List loadDataFromNetwork() {
        Ln.d("Calling Coffee API for fetching Coffee List");
        return getService().coffees();
    }
}

private Coffee.List coffeeList;
private CoffeeListSpiceRequest coffeeListSpiceRequest = new CoffeeListSpiceRequest();

getSpiceManager().execute(coffeeListSpiceRequest, new RequestListener<Coffee.List>() {
            @Override
            public void onRequestFailure(SpiceException spiceException) {
//                Toast.makeText(mContext, "Download Failed", Toast.LENGTH_SHORT).show();
                Toast.makeText(mContext, "Error " + spiceException.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onRequestSuccess(Coffee.List coffees) {
                coffeeList = coffees;
                setAdapter();
            }
        });


```

[CoffeeDetailSpiceRequest](https://bitbucket.org/Creson/cafine/src/5ccf9f5cd653639365e0e249b9f67247ced0d0bd/app/src/main/java/com/creson/test/coffee/network/request/CoffeeDetailSpiceRequest.java?at=master) class is used to request the selected Coffee detail from [Coffee API](https://coffeeapi.percolate.com/api/coffee/{item_id}/)


```
#!android

public class CoffeeDetailSpiceRequest extends RetrofitSpiceRequest<Coffee, CoffeeAPI> {

    private String itemId;

    public CoffeeDetailSpiceRequest(String itemId) {
        super(Coffee.class, CoffeeAPI.class);
        this.itemId = itemId;
    }

    @Override
    public Coffee loadDataFromNetwork() {
        Ln.d("Calling Coffee API for fetching Coffee List");
        return getService().coffee(itemId);
    }
}

    private Coffee localCoffee = new Coffee();

    private CoffeeDetailSpiceRequest coffeeDetailSpiceRequest;

    coffeeDetailSpiceRequest = new CoffeeDetailSpiceRequest(localCoffee.getId());
        getSpiceManager().execute(coffeeDetailSpiceRequest, new RequestListener<Coffee>() {
            @Override
            public void onRequestFailure(SpiceException spiceException) {

            }

            @Override
            public void onRequestSuccess(Coffee coffee) {
                localCoffee = coffee;
                coffeeDetailTextView.setText(localCoffee.getDesc());
            }
        });


```